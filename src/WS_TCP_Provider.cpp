#include "WS_TCP_Provider.h"
#include <Logger.h>
#include <Singleton.h>

#include <utility>
#include "MonitoringManager.h"

using namespace std;
extern shared_ptr<Logger> loggerptr;
extern shared_ptr<MonitoringManager> monitorptr;

WS_TCP_Provider::WS_TCP_Provider(uint8_t IDPostFix, Router* router, uint16_t listenPort,
                                 bool monitorFlag, std::string endpointName)
   : NetProvider(IDPostFix, router), mListenPort(listenPort)
{
   mServer.clear_access_channels(websocketpp::log::alevel::all);
   mServer.set_access_channels(websocketpp::log::alevel::app);
   mServer.init_asio();
   mServer.set_reuse_addr(true);
   mServer.set_message_handler(bind(&WS_TCP_Provider::onWSMessage, this, placeholders::_1, placeholders::_2));
   mServer.set_open_handler(bind(&WS_TCP_Provider::onWSOpen, this, placeholders::_1));
   mServer.set_close_handler(bind(&WS_TCP_Provider::onWSClose, this, placeholders::_1));

   if (monitorFlag)
   {
      if (!endpointName.empty()) endpointName += ".";
      onMonitorIteration();
      monitorptr->addIterationNotifyee(bind(&WS_TCP_Provider::onMonitorIteration, this));
      monitorptr->addMonitoringVariable(endpointName + "websocket.connected_user_count", &mClientConnectionCount);
      monitorptr->addMonitoringVariable(endpointName + "websocket.disconnected_user_count", &mClientDisconnectionCount);
      monitorptr->addMonitoringVariable(endpointName + "websocket.received_packets_count", &mReceivedPacketsCount);
   }
}

void WS_TCP_Provider::onMonitorIteration()
{
   mClientConnectionCount = mClientDisconnectionCount = mReceivedPacketsCount = 0;
}

WS_TCP_Provider::~WS_TCP_Provider()
{
   stop();
}

void WS_TCP_Provider::run()
{
   NetProvider::run();
   error_code ec;
   mServer.listen(mListenPort, ec);

   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Websocket service could not be started on port " << mListenPort << " because: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "Websocket service started on port " << mListenPort;
         loggerptr->flushLogBuffer();
      }

      mServer.start_accept();
      mAsioThread = std::thread(&Server::run, &mServer);
   }
}

void WS_TCP_Provider::stop()
{
   NetProvider::stop();
   mServer.stop();
   if (mAsioThread.joinable())
      mAsioThread.join();

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Websocket service stopped working.";
      loggerptr->flushLogBuffer();
   }
}

void WS_TCP_Provider::onWSOpen(ConnHandler chl)
{
   ServerConnPtr cp = mServer.get_con_from_hdl(std::move(chl));
   onConnectionOpen(cp.get());
}

void WS_TCP_Provider::onWSClose(ConnHandler chl)
{
   ServerConnPtr cp = mServer.get_con_from_hdl(std::move(chl));
   onConnectionClose(cp.get());
}

void WS_TCP_Provider::onWSMessage(ConnHandler chl, ServerMessagePtr msg)
{
   ServerConnPtr cp = mServer.get_con_from_hdl(std::move(chl));
   onPacketReceive(cp.get(), (unsigned char*)msg->get_payload().data(), uint(msg->get_payload().size()));
}

bool WS_TCP_Provider::sendPacket(const void* conn, const unsigned char* payload, uint size)
{
   error_code ec;
   mServer.send(((websocketpp::connection<websocketpp::config::asio>*)conn)->get_handle(),
                payload, size, websocketpp::frame::opcode::text, ec);

   uint cid = getCID(conn);
   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Sending the following message to endpoint \"" << cid
                    << "\" skipped due to the following error :\n"
                    << "Message: " << payload << "\nError: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }

   return !bool(ec);
}

void WS_TCP_Provider::closeConnection(const void* conn, const std::string& reason)
{
   ((websocketpp::connection<websocketpp::config::asio>*)conn)->close(websocketpp::close::status::normal, reason);
}
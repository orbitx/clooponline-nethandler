#include "Socket_TCP_Provider.h"
#include <Logger.h>
#include <Singleton.h>
#include "MonitoringManager.h"

using namespace std;
using namespace boost::asio;
extern shared_ptr<Logger> loggerptr;
extern shared_ptr<MonitoringManager> monitorptr;

std::string bytesToStr(const char* buffer, size_t len)
{
   std::string re = "";
   for (size_t i = 0; i < len; ++i)
   {
      if (i > 0)
         re += " ";
      re += std::to_string(uint(buffer[i]));
   }

   return re;
}

void strToBytes(std::string str, const char** buffer, size_t* len)
{
   //TODO
}

Socket_TCP_Connection::~Socket_TCP_Connection()
{
   if (mSocket.is_open())
   {
      close("socket object removed");
   }
}

void Socket_TCP_Connection::start()
{
   mSocket.set_option(boost::asio::ip::tcp::no_delay(true));
   read();
}

void Socket_TCP_Connection::close(std::string reason)
{
   reason = "connecction is going to be closed because: " + reason;
   write((unsigned char*)reason.data(), reason.size());
   mSocket.close();
}

void Socket_TCP_Connection::read()
{
   //TODO: there will be problem when there are multi thread reading (common buffer)
   auto sizePtr = mBuffer.reserve(2);
   boost::asio::async_read(mSocket, boost::asio::buffer(sizePtr, 2),
                           [this, sizePtr](boost::system::error_code ec, std::size_t length)
                           {
                              if (unlikely((boost::asio::error::eof == ec) ||
                                           (boost::asio::error::connection_reset == ec)))
                              {
                                 mProvider->endpointDisconnected(this);
                              }

                              if (likely(!ec))
                              {
                                 uint flength = (sizePtr[0] << 8) | sizePtr[1];
                                 auto readStart = mBuffer.reserve(flength);
                                 boost::asio::async_read(mSocket, boost::asio::buffer(readStart, flength),
                                                         [this, readStart](boost::system::error_code iec, std::size_t ilength)
                                                         {
                                                            if (unlikely((boost::asio::error::eof == iec) ||
                                                                         (boost::asio::error::connection_reset == iec)))
                                                            {
                                                               mProvider->endpointDisconnected(this);
                                                            }

                                                            if (likely(!iec))
                                                            {
//                                                               cout << "Received: " << bytesToStr(mReadBuffer, flength) << endl;
                                                               mProvider->onPacketReceive(this, readStart, uint(ilength));
                                                               mIdleTimer.expires_from_now(boost::posix_time::time_duration(0, 20, 0));
                                                               read();
                                                            }
                                                         });
                              }
                           });
}

boost::system::error_code Socket_TCP_Connection::write(const unsigned char* payload, size_t len)
{
   static mutex writelock;
   lock_guard<mutex> g(writelock);

   char l[2];
   l[0] = char(len / 256);
   l[1] = char(len % 256);
   boost::system::error_code ec;
   //TODO: make write async
   boost::asio::write(mSocket, boost::asio::buffer(l, 2), ec);

   if (!ec)
   {
      boost::asio::write(mSocket, boost::asio::buffer(payload, len), ec);
      mIdleTimer.expires_from_now(boost::posix_time::time_duration(0, 20, 0));
   }

   return ec;
}

Socket_TCP_Provider::Socket_TCP_Provider(uint8_t IDPostFix, Router* router, uint16_t listenPort,
                                         bool monitorFlag, std::string endpointName)
   : NetProvider(IDPostFix, router), mAcceptor(mIOService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), listenPort)),
     mListenPort(listenPort)
{
   if (monitorFlag)
   {
      if (endpointName != "") endpointName += ".";
      onMonitorIteration();
      monitorptr->addIterationNotifyee(bind(&Socket_TCP_Provider::onMonitorIteration, this));
      monitorptr->addMonitoringVariable(endpointName + "socket.connected_user_count", &mClientConnectionCount);
      monitorptr->addMonitoringVariable(endpointName + "socket.disconnected_user_count", &mClientDisconnectionCount);
      monitorptr->addMonitoringVariable(endpointName + "socket.received_packets_count", &mReceivedPacketsCount);
   }
}

Socket_TCP_Provider::~Socket_TCP_Provider()
{
   stop();
}

void Socket_TCP_Provider::stop()
{
   //TODO: should we close all socket connections too?
   NetProvider::stop();
   mIOService.stop();
   mAcceptor.close();
   if (mIOServiceThread.joinable())
      mIOServiceThread.join();

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "TCP socket service stopped working.";
      loggerptr->flushLogBuffer();
   }
}

void Socket_TCP_Provider::onMonitorIteration()
{
   mClientConnectionCount = mClientDisconnectionCount = mReceivedPacketsCount = 0;
}

void Socket_TCP_Provider::run()
{
   NetProvider::run();
   acceptNewConnection();
   mIOServiceThread = thread((size_t(io_service::*)())(&io_service::run), &mIOService);

   if (mIOService.stopped())
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "TCP Socket service could not be started";
         loggerptr->flushLogBuffer();
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "TCP Socket service started on port " << mListenPort;
         loggerptr->flushLogBuffer();
      }
   }
}

void Socket_TCP_Provider::acceptNewConnection()
{
   Socket_TCP_Connection* newconn = new Socket_TCP_Connection(mIOService, this);

   mAcceptor.async_accept(newconn->getSocket(),
                          [this, newconn](boost::system::error_code ec)
                          {
                             if (!ec)
                             {
                                newconn->start();
                                onConnectionOpen(newconn);
                                acceptNewConnection();
                             }
                          });
}

bool Socket_TCP_Provider::sendPacket(const void* conn, const unsigned char* payload, uint size)
{
   boost::system::error_code ec = ((Socket_TCP_Connection*)conn)->write(payload, size);
   uint cid = getCID(conn);

   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Sending the following message to endpoint \"" << cid
                    << "\" skipped due to the following error :\n"
                    << "Message: " << payload << "\nError: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }

   return !bool(ec);
}

void Socket_TCP_Provider::closeConnection(const void* conn, const std::string& reason)
{
   ((Socket_TCP_Connection*)conn)->close(reason);
}

void Socket_TCP_Provider::endpointDisconnected(Socket_TCP_Connection* conn)
{
   onConnectionClose(conn);
}
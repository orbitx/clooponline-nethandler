#include "Exception.h"
#include "Router.h"
#include "NetProvider.h"
#include <Logger.h>
#include "clpbuffer.h"
#include <Singleton.h>
#include <boost/pool/object_pool.hpp>
#include <utility>
#include "AAAChecker.h"

using namespace std;
extern shared_ptr<Logger> loggerptr;

thread_local PacketParserFactory* t_packet_factory;
thread_local IBuffer* t_message_buffer;

Router::Router(ushort numOfThread) :
   mMagicCallByMagic(mMagicCallInfos.get<MagicCallInfo::ByMagic>()),
   mMagicCallByTimeoutIndex(mMagicCallInfos.get<MagicCallInfo::ByTimeoutIndex>()),
   mAAAChecker(make_unique<AAAChecker>(this)),
   mThreadPool(numOfThread)
{
   mLastMagic = 0;
   mDCHandler = nullptr;
   mIdentifyHandler = nullptr;
}

Router::~Router()
{
   shutdown();
   mThreadPool.StopWorking();
}

void Router::runNetProviders()
{
   for (auto& p : mNetProviders)
   {
      p->run();
   }
}

void Router::shutdown()
{
   for (auto& p : mNetProviders)
   {
      if (p->getState() == NetProvider::ServingState::ACCEPTING_NEW_CONNECTIONS)
         p->stop();
   }

   clearNetProviders();
}

void Router::clearNetProviders()
{
   mNetProviders.clear();
}

void Router::cleanShutdown()
{
   for (auto& p : mNetProviders)
   {
      p->cleanShutdown();
   }

   for (auto& p : mNetProviders)
   {
      p->waitForCleanShutdown();
   }

   shutdown();
}

bool Router::sendPacket(uint clientID, PacketParser* packet)
{
   //TODO: make this to be handled async like receiving packets
   auto res = packet->serialize(t_message_buffer);
   bool re = sendPacket(clientID, res.first, res.second);

   if (re)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_DEBUG))
      {
         *loggerptr << "The following packet has been sent to endpoint \"" << clientID << "\":"
                    << "\n\t" << packet;
         loggerptr->flushLogBuffer();
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_DEBUG))
      {
         *loggerptr << "Sending the following packet to endpoint \"" << clientID << "\" failed:"
                    << "\n" << packet;
         loggerptr->flushLogBuffer();
      }
   }

   return re;
}

bool Router::sendPacket(const ProfilePtr client, PacketParser* packet)
{
   return sendPacket(client->CID, packet);
}

void Router::errorEncountered(const char* desc)
{
   if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
   {
      *loggerptr << "Encountered error: " << desc;
      loggerptr->flushLogBuffer();
   }
}

ProfilePtr Router::getProfile(uint CID)
{
   return mAAAChecker->getProfile(CID);
}

bool Router::sendPacket(uint clientID, const unsigned char* payload, uint size)
{
   return getNetProvider(clientID)->sendPacket(clientID, payload, size);
}

bool Router::sendPacket(ProfilePtr client, const unsigned char* payload, uint size)
{
   return sendPacket(client->CID, payload, size);
}

void Router::closeConnection(ProfilePtr client, string reason)
{
   getNetProvider(client->CID)->closeConnection(client->CID, std::move(reason));
}

ulong Router::sendRequest(ProfilePtr client, PacketParser* reqPacket, PacketHandler toCall,
                          PacketHandler exceHandler, ulong jobGroupID, uint timeout, TimeoutHandler timeoutHandler)
{
   ulong omagic = ++mLastMagic;

   reqPacket->setOMagic(omagic);
   sendPacket(client, reqPacket);

   ulong workind = mThreadPool.AddEvent(bind(&Router::requestTimeout, this, client, omagic, timeoutHandler), timeout);
   mMagicCallInfos.insert(MagicCallInfo(omagic, std::move(toCall), workind, jobGroupID, std::move(exceHandler)));

   reqPacket->eraseOMagic();
   return workind;
}

void Router::setTypeHandler(int type, PacketHandler handler, ulong jobGroupID)
{
   mTypetoHandler[type] = make_pair(handler, jobGroupID);
}

void Router::timeoutDone(ulong workind)
{
   mMagicCallLock.lock();
   auto it = mMagicCallByTimeoutIndex.find(workind);
   if (it != mMagicCallByTimeoutIndex.end())
   {
      mMagicCallByTimeoutIndex.erase(it);
   }
   mMagicCallLock.unlock();

   mThreadPool.RemoveEvent(workind);
}

void Router::magicDone(ulong magic)
{
   auto it = mMagicCallByMagic.find(magic);
   if (it != mMagicCallByMagic.end())
   {
      mMagicCallByMagic.erase(it);
   }

   mThreadPool.RemoveEvent(it->TimeoutIndex);
}

void Router::processRequest(uint clientID, const unsigned char* payload, uint size)
{
   PacketParser* reqPacket = t_packet_factory->createOne();
   PacketParser* repPacket = t_packet_factory->createOne();

   //TODO: there is overhead in using shared_ptr this much!
   ProfilePtr client = mAAAChecker->getProfile(clientID);
   try
   {
      reqPacket->deserialize(payload, size);
      if (loggerptr->lockBuffer(LOG_SEVERITY_DEBUG))
      {
         *loggerptr << "Processing the following packet from endpoint \"" << clientID << "\" :"
                    << "\n\t" << reqPacket;
         loggerptr->flushLogBuffer();
      }

      if (client == nullptr || !(client->IsConnected))
      {
         if (loggerptr->lockBuffer(LOG_SEVERITY_WARNING))
         {
            *loggerptr << "A request from endpoint \"" << clientID
                       << "\" skipped because its connection has been closed.";
            loggerptr->flushLogBuffer();
         }
      }
      else
      {
         if (reqPacket->hasEMagic())
         {
            ulong emagic = reqPacket->getEMagic();
            repPacket->setEMagic(emagic);
         }

         if (reqPacket->hasOMagic())
         {
            ulong omagic = reqPacket->getOMagic();
            auto it = mMagicCallByMagic.find(omagic);
            if (it != mMagicCallByMagic.end())
            {
               if (reqPacket->isException())
               {
                  if (it->ExceptionHandler)
                  {
                     //TODO: if exception handler throws exception packets will not be freed
                     mThreadPool.AddJob(it->JobGroupID, it->ExceptionHandler, client, reqPacket, repPacket);
                  }
                  else
                  {
                     if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
                     {
                        *loggerptr << "Received the following exception: " << reqPacket->getErrorDescription();
                        loggerptr->flushLogBuffer();
                     }
                  }
               }
               else
               {
                  mThreadPool.RemoveEvent(it->TimeoutIndex);
                  mThreadPool.AddJob(it->JobGroupID, &Router::packetCallWrapper, this, it->ToCall, client, reqPacket,
                                     repPacket);
               }
            }
            else
            {
               if (loggerptr->lockBuffer(LOG_SEVERITY_WARNING))
               {
                  *loggerptr << "Omagic did not recognized. Respond of message with omagic \"" << omagic
                             << "\" skipped.";
                  loggerptr->flushLogBuffer();
               }
            }
         }
         else if (!(client->IsAuthenticated))
         {
            mAAAChecker->onPacketFromUnidentified(client, reqPacket, repPacket);
         }
         else
         {
            if (reqPacket->isException())
            {
               if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
               {
                  *loggerptr << "Received the following exception: " << reqPacket->getErrorDescription();
                  loggerptr->flushLogBuffer();
               }

               if (mGeneralExceptionHandler)
               {
                  mThreadPool.AddJob(mGeneralExceptionJobGroup, mGeneralExceptionHandler, client, reqPacket, repPacket);
               }
            }
            else
            {
               int msgType = reqPacket->getMessageType();
               auto callable = mTypetoHandler.find(msgType);
               if (callable != mTypetoHandler.end())
               {
                  mThreadPool.AddJob(callable->second.second, &Router::packetCallWrapper, this, callable->second.first,
                                     client, reqPacket, repPacket);
               }
               else
               {
                  if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
                  {
                     *loggerptr << "Message type \"" << msgType << "\" unidentified.";
                     loggerptr->flushLogBuffer();
                  }

                  repPacket->setAsException("unidentified message");
                  sendPacket(client, repPacket);
               }
            }
         }
      }
   }
   catch (CloopException& e)
   {
      EXCEPTION_HAPPENED(client, e.getCode())
   }
   catch (exception& e)
   {
      EXCEPTION_HAPPENED(client, e.what())
   }
}

void Router::packetCallWrapper(Router::PacketHandler handler, ProfilePtr client, PacketParser* reqPacket,
                               PacketParser* repPacket)
{
   try
   {
      handler(client, reqPacket, repPacket);
   }
   catch (CloopException& e)
   {
      EXCEPTION_HAPPENED(client, e.getCode())
   }
   catch (exception& e)
   {
      EXCEPTION_HAPPENED(client, e.what())
   }
}

void Router::callWrapper(std::function<void()> handler, ProfilePtr client, PacketParser* reqPacket,
                         PacketParser* repPacket)
{
   try
   {
      handler();
   }
   catch (CloopException& e)
   {
      EXCEPTION_HAPPENED(client, e.getCode())
   }
   catch (exception& e)
   {
      EXCEPTION_HAPPENED(client, e.what())
   }
}

void Router::packetReceived(uint clientID, const unsigned char* payload, uint size)
{
   mThreadPool.AddJob(&Router::processRequest, this, clientID, payload, size);
   ++mReceivedPacketsCount;
}

void Router::connected(uint clientID)
{
   ++mClientConnectionCount;

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "An endpoint connected and got the ID \"" << clientID << "\".";
      loggerptr->flushLogBuffer();
   }

   mAAAChecker->connected(clientID);
}

void Router::identified(ProfilePtr client)
{
   if (mIdentifyHandler)
      mThreadPool.AddJob(mIdentifyHandlerJobGroup, mIdentifyHandler, std::move(client));
}

void Router::disconnected(uint clientID)
{
   ++mClientDisconnectionCount;
   if (mDCHandler)
      mThreadPool.AddJob(mDCHandlerJobGroup, mDCHandler, mAAAChecker->getProfile(clientID));

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Endpoint \"" << clientID << "\" disconnected. ";
      loggerptr->flushLogBuffer();
   }

   mAAAChecker->disconnected(clientID);
}

void Router::requestTimeout(ProfilePtr client, ulong magic, TimeoutHandler timeoutHandler)
{
   auto it = mMagicCallByMagic.find(magic);
   if (it == mMagicCallByMagic.end())
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_WARNING))
      {
         *loggerptr << "Unexpectedly magic was not found in requenst timeout method.";
         loggerptr->flushLogBuffer();
      }
      return;
   }

   mThreadPool.RemoveEvent(it->TimeoutIndex);
   ulong jg = it->JobGroupID;
   mMagicCallByMagic.erase(it);

   if (timeoutHandler)
   {
      mThreadPool.AddJob(jg, timeoutHandler);
   }
   else
   {
      closeConnection(std::move(client), "response timed out");
   }
}
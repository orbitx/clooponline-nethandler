#include <Singleton.h>
#include <Logger.h>
#include "Client_Socket_TCP_Provider.h"

using boost::asio::ip::tcp;
using boost::asio::io_service;
using namespace std;

extern shared_ptr<Logger> loggerptr;

std::string bytesToStr(const char* buffer, size_t len)
{
   std::string re = "";
   for (size_t i = 0; i < len; ++i)
   {
      if (i > 0)
         re += " ";
      re += std::to_string(uint(buffer[i]));
   }

   return re;
}

void strToBytes(std::string str, const char** buffer, size_t* len)
{
   //TODO
}

Client_Socket_TCP_Provider::Client_Socket_TCP_Provider(uint8_t IDPostFix, Router* router,
                                                       std::string hostip, uint16_t hostport)
   : NetProvider(IDPostFix, router), mSocket(mIOService),
     mHostIP(hostip), mHostPort(hostport), mBuffer(102400)
{
}

Client_Socket_TCP_Provider::~Client_Socket_TCP_Provider()
{
   if (mSocket.is_open())
   {
      mSocket.close();
   }
}

void Client_Socket_TCP_Provider::stop()
{
   NetProvider::stop();
   mIOService.stop();
   mSocket.close();
   if (mIOServiceThread.joinable())
      mIOServiceThread.join();

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "TCP socket connection stopped working.";
      loggerptr->flushLogBuffer();
   }
}

void Client_Socket_TCP_Provider::run()
{
   NetProvider::run();
   boost::system::error_code ec;
   tcp::resolver resolver(mIOService);
   boost::asio::connect(mSocket, resolver.resolve({mHostIP, to_string(mHostPort)}), ec);

   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "TCP Socket connection could not be established to " << mHostIP << ":" << mHostPort
                    << " because: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "TCP Socket connection established to " << mHostIP << ":" << mHostPort;
         loggerptr->flushLogBuffer();
      }

      onConnectionOpen(this);
      mSocket.set_option(boost::asio::ip::tcp::no_delay(true));
      read();
      mIOServiceThread = thread((size_t(io_service::*)())(&io_service::run), &mIOService);
   }
}

bool Client_Socket_TCP_Provider::sendPacket(const void* conn, const unsigned char* payload, uint size)
{
   boost::system::error_code ec = write(payload, size);

   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Sending the following message to host skipped due to the following error :\n"
         << "Message: " << payload << "\nError: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }

   return !bool(ec);
}

void Client_Socket_TCP_Provider::closeConnection(const void* conn, const std::string& reason)
{
   string rea = "connecction is going to be closed because: " + reason;
   write((unsigned char*)rea.data(), rea.size());
   mSocket.close();
}

void Client_Socket_TCP_Provider::read()
{
   auto sizePtr = mBuffer.reserve(2);
   boost::asio::async_read(mSocket, boost::asio::buffer(sizePtr, 2),
                           [this, sizePtr](boost::system::error_code ec, std::size_t length)
                           {
                              if ((boost::asio::error::eof == ec) ||
                                  (boost::asio::error::connection_reset == ec))
                              {
                                 onConnectionClose(this);
                              }

                              if (!ec)
                              {
                                 uint flength = (sizePtr[0] << 8) | sizePtr[1];
                                 auto readStart = mBuffer.reserve(flength);
                                 boost::asio::async_read(mSocket, boost::asio::buffer(readStart, flength),
                                                         [this, readStart](boost::system::error_code iec, std::size_t ilength)
                                                         {
                                                            if ((boost::asio::error::eof == iec) ||
                                                                (boost::asio::error::connection_reset == iec))
                                                            {
                                                               onConnectionClose(this);
                                                            }

                                                            if (!iec)
                                                            {
//                                                               cout << "Received: " << bytesToStr(mReadBuffer, flength) << endl;
                                                               onPacketReceive(this, readStart, uint(ilength));
                                                               read();
                                                            }
                                                         });
                              }
                           });
}

boost::system::error_code Client_Socket_TCP_Provider::write(const unsigned char* payload, size_t len)
{
   static mutex writelock;
   lock_guard<mutex> g(writelock);

   char l[2];
   l[0] = char(len / 256);
   l[1] = char(len % 256);
   boost::system::error_code ec;
   boost::asio::write(mSocket, boost::asio::buffer(l, 2), ec);
   if (!ec)
      boost::asio::write(mSocket, boost::asio::buffer(payload, len), ec);

   return ec;
}
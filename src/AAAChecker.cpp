#include <AAAChecker.h>
#include <Router.h>
#include <Exception.h>

using namespace std;

shared_ptr<const Profile> AAAChecker::getProfile(uint CID)
{
   lock_guard<mutex> l(mProfileLock);
   auto it = mProfileMap.find(CID);
   if (it != mProfileMap.end())
      return it->second;

   auto re = make_shared<Profile>(0);
   return re;
}

void AAAChecker::connected(uint CID)
{
   mProfileLock.lock();
   auto it = mProfileMap.insert(make_pair(CID, mProfileFactory->createOne(CID)));
   mProfileLock.unlock();
   connectHandler(it.first->second);
}

void AAAChecker::setAuthFlag(shared_ptr<const Profile> client, bool flag)
{
   lock_guard<mutex> l(mProfileLock);
   auto it = mProfileMap.find(client->CID);
   if (it == mProfileMap.end())
      return;
   it->second->IsAuthenticated = flag;
   
   if (flag) mRouter->identified(it->second);
}

void AAAChecker::disconnected(uint CID)
{
   mProfileLock.lock();
   auto it = mProfileMap.find(CID);
   if (it != mProfileMap.end())
   {
      shared_ptr<Profile> p = it->second;
      p->IsConnected = false;
      mProfileMap.erase(it);
      mProfileLock.unlock();
      disconnectHandler(p);
   }
   else
      mProfileLock.unlock();
}

void AAAChecker::connectHandler(std::shared_ptr<const Profile> client)
{
   const_cast<Profile*>(client.get())->IsAuthenticated = true;
   mRouter->identified(client);
}

void AAAChecker::onPacketFromUnidentified(std::shared_ptr<const Profile> client, PacketParser* reqPacket, PacketParser* repPacket)
{
   CloopException e(NOT_IDENTIFIED_ERROR_CODE);
   if (reqPacket->hasOMagic())
      repPacket->setOMagic(reqPacket->getOMagic());
   repPacket->setAsException(e.getCode());
   mRouter->sendPacket(client, repPacket);
}
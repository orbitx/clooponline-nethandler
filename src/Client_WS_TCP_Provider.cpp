#include "Client_WS_TCP_Provider.h"
#include <Logger.h>
#include <Singleton.h>

using namespace std;
extern shared_ptr<Logger> loggerptr;

Client_WS_TCP_Provider::Client_WS_TCP_Provider(uint8_t IDPostFix, Router* router, std::string hostip, uint16_t hostport)
   : NetProvider(IDPostFix, router), mHostIP(hostip), mHostPort(hostport)
{
   mClient.clear_access_channels(websocketpp::log::alevel::all);
   mClient.set_access_channels(websocketpp::log::alevel::app);
   mClient.init_asio();
   mClient.set_reuse_addr(true);
   mClient.set_message_handler(bind(&Client_WS_TCP_Provider::onWSMessage, this, placeholders::_1, placeholders::_2));
   mClient.set_open_handler(bind(&Client_WS_TCP_Provider::onWSOpen, this, placeholders::_1));
   mClient.set_close_handler(bind(&Client_WS_TCP_Provider::onWSClose, this, placeholders::_1));
}

Client_WS_TCP_Provider::~Client_WS_TCP_Provider()
{
   stop();
}

void Client_WS_TCP_Provider::run()
{
   NetProvider::run();

   stringstream ss;
   ss << "ws://" << mHostIP << ":" << mHostPort;
   websocketpp::lib::error_code ec;
   mClientConn = mClient.get_connection(ss.str(), ec);

   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Could not establish websocket connection to " << ss.str() << " because: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "Websocket connection to " << ss.str() << " established.";
         loggerptr->flushLogBuffer();
      }

      mClient.connect(mClientConn);
      mAsioThread = thread(&Client::run, &mClient);
   }
}

void Client_WS_TCP_Provider::stop()
{
   NetProvider::stop();
   mClient.stop();
   mClient.stop_listening();
   if (mAsioThread.joinable())
      mAsioThread.join();

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Websocket connection closed.";
      loggerptr->flushLogBuffer();
   }
}

bool Client_WS_TCP_Provider::sendPacket(const void* conn, const unsigned char* payload, uint size)
{
   error_code ec;
   mClient.send(((websocketpp::connection<websocketpp::config::asio_client>*)conn)->get_handle(),
                payload, size, websocketpp::frame::opcode::text, ec);

   uint sid = getCID(conn);
   if (ec)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Sending the following message to endpoint \"" << sid
                    << "\" skipped due to the following error :\n"
                    << "Message: " << payload << "\nError: " << ec.message();
         loggerptr->flushLogBuffer();
      }
   }

   return !bool(ec);
}

void Client_WS_TCP_Provider::closeConnection(const void* conn, const std::string& reason)
{
   ((websocketpp::connection<websocketpp::config::asio_client>*)conn)->close(websocketpp::close::status::normal, reason);
}

void Client_WS_TCP_Provider::onWSOpen(ConnHandler chl)
{
   ClientConnPtr cp = mClient.get_con_from_hdl(chl);
   onConnectionOpen(cp.get());
}

void Client_WS_TCP_Provider::onWSClose(ConnHandler chl)
{
   ClientConnPtr cp = mClient.get_con_from_hdl(chl);
   onConnectionClose(cp.get());
}

void Client_WS_TCP_Provider::onWSMessage(ConnHandler chl, ClientMessagePtr msg)
{
   ClientConnPtr cp = mClient.get_con_from_hdl(chl);
   onPacketReceive(cp.get(), (unsigned char*)msg->get_payload().data(), uint(msg->get_payload().size()));
}
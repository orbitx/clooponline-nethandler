#pragma once

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>
#include <thread>
#include "NetProvider.h"

typedef websocketpp::client<websocketpp::config::asio_client> Client;
typedef Client::message_ptr ClientMessagePtr;
typedef websocketpp::connection_hdl ConnHandler;
typedef Client::connection_ptr ClientConnPtr;

class Client_WS_TCP_Provider : public NetProvider
{
public:
   Client_WS_TCP_Provider(uint8_t IDPostFix, Router* router, std::string hostip, uint16_t hostport);
   ~Client_WS_TCP_Provider() override;

   void run() override;
   void stop() override;

private:

   bool sendPacket(const void* conn, const unsigned char* payload, uint size) override;
   void closeConnection(const void* conn, const std::string& reason) override;

   Client mClient;
   ClientConnPtr mClientConn;
   std::string mHostIP;
   uint16_t mHostPort;
   std::thread mAsioThread;

   //websocket events
   void onWSMessage(ConnHandler chl, ClientMessagePtr msg);
   void onWSOpen(ConnHandler chl);
   void onWSClose(ConnHandler chl);
};
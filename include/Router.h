#pragma once

#include <map>
#include <functional>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <thread>
#include <utility>
#include <ThreadPool.h>
#include <clpbuffer.h>
#include "PacketParser.h"

class NetProvider;
class AAAChecker;
class PacketParserFactory;
class Profile;
typedef std::shared_ptr<const Profile> ProfilePtr;

//TODO: not a good idea!
#define NOT_IDENTIFIED_ERROR_CODE 2

#define EXCEPTION_HAPPENED(CLIENTID, MSG) \
   if (reqPacket->hasOMagic()) \
      repPacket->setOMagic(reqPacket->getOMagic()); \
   repPacket->setAsException(MSG); \
   sendPacket(CLIENTID, repPacket);

extern thread_local PacketParserFactory* t_packet_factory;
extern thread_local IBuffer* t_message_buffer;

class Router
{
public:
   typedef std::function<void(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)> PacketHandler;
   typedef std::function<void()> TimeoutHandler;
   typedef std::function<void(ProfilePtr client)> DisconnectHandler;
   typedef std::function<void(ProfilePtr client)> IdentifyHandler;

protected:
   struct Request
   {
      Request(uint clientID, const char* payload, uint size) : mClientID(clientID), mReqPayload(payload), mSize(size)
      {
      }
      uint mClientID;
      const char* mReqPayload;
      uint mSize;
   };

   struct MagicCallInfo
   {
      MagicCallInfo (ulong magic, PacketHandler call, ulong timeoutInd, ulong jobGroupID, PacketHandler exceHandler)
         : Magic(magic), ToCall(std::move(call)), TimeoutIndex(timeoutInd), JobGroupID(jobGroupID), ExceptionHandler(
         std::move(exceHandler)) { }
      PacketHandler ToCall;
      ulong TimeoutIndex;
      ulong Magic;
      ulong JobGroupID;
      PacketHandler ExceptionHandler;

      struct ByMagic {};
      struct ByTimeoutIndex {};
   };

   typedef boost::multi_index_container<MagicCallInfo,
      boost::multi_index::indexed_by<
         boost::multi_index::ordered_unique<
            boost::multi_index::tag<MagicCallInfo::ByMagic>,
            boost::multi_index::member<MagicCallInfo, ulong, &MagicCallInfo::Magic>
         >,
         boost::multi_index::ordered_unique<
            boost::multi_index::tag<MagicCallInfo::ByTimeoutIndex>,
            boost::multi_index::member<MagicCallInfo, ulong, &MagicCallInfo::TimeoutIndex>
         >
      >
   > MagicCallContainer;

   typedef MagicCallContainer::index<MagicCallInfo::ByMagic>::type MagicCallByMagic;
   typedef MagicCallContainer::index<MagicCallInfo::ByTimeoutIndex>::type MagicCallByTimeoutIndex;

public:

   explicit Router(ushort numOfThread = 8);
   ulong sendRequest(ProfilePtr client, PacketParser* reqPacket,
                     PacketHandler toCall, PacketHandler exceHandler = 0, ulong jobGroupID = 0, uint timeout = 30,
                     TimeoutHandler timeoutHandler = nullptr);
   bool sendPacket(ProfilePtr client, PacketParser* packet);
   bool sendPacket(uint clientID, PacketParser* packet);
   bool sendPacket(ProfilePtr client, const unsigned char* payload, uint size);
   bool sendPacket(uint clientID, const unsigned char* payload, uint size);
   void closeConnection(ProfilePtr client, std::string reason);
   void setTypeHandler(int type, PacketHandler handler, ulong jobGroupID = 1);
   void setDCHandler(DisconnectHandler handler, ulong jobGroupID = 1) { mDCHandler = std::move(handler); mDCHandlerJobGroup = jobGroupID; }
   void setIdentifyHandler(IdentifyHandler handler, ulong jobGroupID = 1) { mIdentifyHandler = std::move(handler); mIdentifyHandlerJobGroup = jobGroupID; }
   void setGeneralExceptionHandler(PacketHandler handler, ulong jobGroupID = 1) { mGeneralExceptionHandler = std::move(
         handler); mGeneralExceptionJobGroup = jobGroupID; }
   void identified(ProfilePtr client);
   void errorEncountered(const char* desc);
   ProfilePtr getProfile(uint CID);
   PacketParser* createPacket() { return t_packet_factory->createOne(); }

   void timeoutDone(ulong workind);
   void magicDone(ulong magic);

   //TODO: use tuple and forward inputs to buffer and factory constructors
   template<class BUFFER, class PFACTORY>
   void run(bool isServer)
   {
      if (!mThreadPool.isStarted())
      {
         mThreadPool.Run();
         mThreadPool.RunOnAllThreads([this, isServer](){
            defineThreadResources<BUFFER, PFACTORY>(isServer);
         });
      }
      runNetProviders();
   }

   template<class BUFFER, class PFACTORY>
   void defineThreadResources(bool isServer)
   {
      //TODO: buffer size as thread resource should be configurable
      t_message_buffer = new BUFFER(10240);
      t_packet_factory = new PFACTORY(isServer);
   }

   void cleanShutdown();
   void shutdown();
   void clearNetProviders();

   template <class ProviderType, class... ArgsType>
   void addNetProvider(ArgsType... args)
   {
      if (mNetProviders.size() >= 99)
         throw std::runtime_error("Router cannot accept more than 100 network providers.");

      std::unique_ptr<NetProvider> provider(new ProviderType(mNetProviders.size(), this, args...));

      mNetProviders.push_back(std::move(provider));
   }

   template <class T, class... ArgsType>
   void addAAAChecker(ArgsType... args)
   {
      mAAAChecker.reset(new T(this, args...));
   };

   void connected(uint clientID);
   void disconnected(uint clientID);
   void packetReceived(uint clientID, const unsigned char* payload, uint size);
   void packetCallWrapper(PacketHandler handler, ProfilePtr client, PacketParser* reqPacket,
                          PacketParser* repPacket);
   void callWrapper(std::function<void()> handler, ProfilePtr client, PacketParser* reqPacket,
                    PacketParser* repPacket);
   virtual ~Router();

   ThreadPool  mThreadPool;

protected:
   std::vector<std::unique_ptr<NetProvider>> mNetProviders;
   std::unique_ptr<AAAChecker>    mAAAChecker;

   //Monitoring variables
   std::atomic<uint> mReceivedPacketsCount;
   std::atomic<uint> mClientConnectionCount;
   std::atomic<uint> mClientDisconnectionCount;

   MagicCallContainer       mMagicCallInfos;
   MagicCallByMagic&        mMagicCallByMagic;
   MagicCallByTimeoutIndex& mMagicCallByTimeoutIndex;
   std::mutex mMagicCallLock;

   //type --> (handler, jobGroupID)
   std::map<int, std::pair<PacketHandler, ulong>> mTypetoHandler;
   DisconnectHandler mDCHandler;
   ulong             mDCHandlerJobGroup;
   IdentifyHandler   mIdentifyHandler;
   ulong             mIdentifyHandlerJobGroup;
   PacketHandler     mGeneralExceptionHandler;
   ulong             mGeneralExceptionJobGroup;

   std::atomic<ulong> mLastMagic;

   void runNetProviders();
   NetProvider* getNetProvider(uint clientID)
   {
      return mNetProviders[clientID % 100].get();
   }

   void processRequest(uint clientID, const unsigned char* payload, uint size);
   void requestTimeout(ProfilePtr client, ulong magic, TimeoutHandler timeoutHandler);
};
#pragma once

#include "NetProvider.h"
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <clpbuffer.h>

class Socket_TCP_Provider;

class Socket_TCP_Connection
{
public:
   Socket_TCP_Connection(boost::asio::io_service& IOService, Socket_TCP_Provider* provider)
      : mSocket(IOService), mProvider(provider), mIdleTimer(IOService), mBuffer(102400)
   {
      mIdleTimer.expires_from_now(boost::posix_time::time_duration(0, 20, 0));
      mIdleTimer.async_wait([this](const boost::system::error_code& error)
                            {
                               if (error != boost::asio::error::operation_aborted)
                               {
                                  close("idle connection");
                               }
                            });
   }

   virtual ~Socket_TCP_Connection();
   void start();
   void close(std::string reason);
   boost::system::error_code write(const unsigned char* payload, size_t len);
   boost::asio::ip::tcp::socket& getSocket()
   {
      return mSocket;
   }

private:
   void read();

   boost::asio::ip::tcp::socket mSocket;
   boost::asio::deadline_timer mIdleTimer;
   Socket_TCP_Provider* mProvider;
   clpbuffer mBuffer;
};

class Socket_TCP_Provider : public NetProvider
{
public:
   Socket_TCP_Provider(uint8_t IDPostFix, Router* router, uint16_t listenPort,
                       bool monitorFlag = false, std::string endpointName = "");
   virtual ~Socket_TCP_Provider();

   virtual void run();
   virtual void stop();
   void endpointDisconnected(Socket_TCP_Connection* conn);

private:
   virtual bool sendPacket(const void* conn, const unsigned char* payload, uint size);
   virtual void closeConnection(const void* conn, const std::string& reason);
   void acceptNewConnection();
   void onMonitorIteration();

   //TODO: should we have list of IOServices to support multi threaded reading from connections?
   boost::asio::io_service mIOService;
   boost::asio::ip::tcp::acceptor mAcceptor;
   std::thread mIOServiceThread;
   uint16_t mListenPort;
};

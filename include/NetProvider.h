#pragma once

#include <map>
#include <atomic>
#include <boost/bimap.hpp>
#include <mutex>
#include "Router.h"

class NetProvider
{
   typedef boost::bimap<uint, const void*> IDConnType;

public:
   enum ServingState
   {
      NOT_RUNNING,
      ACCEPTING_NEW_CONNECTIONS,
      REJECTING_NEW_CONNECTIONS_SERVING_EXISTING_CONNECTIONS
   };

   NetProvider(uint8_t IDPostFix, Router* router)
      : mIDPostFix(IDPostFix), mRouter(router), mLastClientID(0)
   {
      if (IDPostFix >= 100)
         throw std::runtime_error("ID postfix should have maximum of two digits.");

      mServingState = NOT_RUNNING;
   }

   virtual ~NetProvider() { }

   bool isConnected(uint clientID)
   {
      bool re = true;
      mMapLock.lock();
      auto it = mCID_ConnMap.left.find(clientID);
      if (it == mCID_ConnMap.left.end())
      {
         re = false;
      }
      mMapLock.unlock();
      return re;
   }

   bool sendPacket(uint clientID, const unsigned char* payload, uint len)
   {
      const void* conn = getConnection(clientID);
      if (!conn) return false;
      return sendPacket(conn, payload, len);
   }

   void closeConnection(uint clientID, std::string reason)
   {
      const void* conn = getConnection(clientID);
      if (!conn) return;
      onConnectionClose(conn);
      closeConnection(conn, reason);
   }

   void cleanShutdown()
   {
      mServingState = REJECTING_NEW_CONNECTIONS_SERVING_EXISTING_CONNECTIONS;
   }

   void waitForCleanShutdown()
   {
      if (mServingState != REJECTING_NEW_CONNECTIONS_SERVING_EXISTING_CONNECTIONS)
         cleanShutdown();

      while (mCID_ConnMap.size() > 0)
      {
         std::this_thread::sleep_for(std::chrono::seconds(1));
      }

      stop();
   }

   void onPacketReceive(const void* conn, const unsigned char* payload, uint len)
   {
      ++mReceivedPacketsCount;
      try
      {
         mRouter->packetReceived(getCID(conn), payload, len);
      }
      catch (std::exception& e)
      {
         mRouter->errorEncountered(e.what());
      }
   }

   ServingState getState() { return mServingState; }

   virtual void run() { mServingState = ACCEPTING_NEW_CONNECTIONS; }
   virtual void stop() { mServingState = NOT_RUNNING; }

protected:
   virtual bool sendPacket(const void* conn, const unsigned char* payload, uint size) = 0;
   virtual void closeConnection(const void* conn, const std::string& reason) = 0;

   uint getNextCID() { return ++mLastClientID * 100 + mIDPostFix; }

   uint getCID(const void* conn)
   {
      mMapLock.lock();
      auto it = mCID_ConnMap.right.find(conn);
      if (it == mCID_ConnMap.right.end())
      {
         mMapLock.unlock();
         throw std::runtime_error("Getting CID failed: connection does not belong to this NetProvider.");
      }
      uint cid = it->second;
      mMapLock.unlock();
      return cid;
   }

   const void* getConnection(uint clientID)
   {
      mMapLock.lock();
      auto it = mCID_ConnMap.left.find(clientID);
      if (it == mCID_ConnMap.left.end())
      {
         mMapLock.unlock();
         return nullptr;
      }
      const void* cp = it->second;
      mMapLock.unlock();
      return cp;
   }

   void onConnectionOpen(const void* conn)
   {
      if (mServingState != ACCEPTING_NEW_CONNECTIONS)
      {
         closeConnection(conn, "out of service");
      }

      uint id = getNextCID();
      mMapLock.lock();
      mCID_ConnMap.insert(typename IDConnType::value_type(id, conn));
      mMapLock.unlock();

      ++mClientConnectionCount;
      mRouter->connected(id);
   }

   void onConnectionClose(const void* conn)
   {
      mMapLock.lock();
      auto it = mCID_ConnMap.right.find(conn);
      if (it == mCID_ConnMap.right.end())
      {
         mMapLock.unlock();
         return;
      }
      uint cid = it->second;
      mCID_ConnMap.right.erase(it);
      mMapLock.unlock();

      ++mClientDisconnectionCount;
      mRouter->disconnected(cid);
   }

   ServingState mServingState;
   uint8_t mIDPostFix;
   std::atomic<uint> mLastClientID;
   IDConnType mCID_ConnMap;
   std::mutex mMapLock;
   uint mReceivedPacketsCount;
   uint mClientConnectionCount;
   uint mClientDisconnectionCount;
   Router* mRouter;
};
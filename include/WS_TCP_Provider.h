#pragma once

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <thread>
#include "NetProvider.h"

typedef websocketpp::server<websocketpp::config::asio> Server;
typedef Server::message_ptr ServerMessagePtr;
typedef websocketpp::connection_hdl ConnHandler;
typedef Server::connection_ptr ServerConnPtr;

class WS_TCP_Provider : public NetProvider
{
public:
   WS_TCP_Provider(uint8_t IDPostFix, Router* router, uint16_t listenPort,
                   bool monitorFlag = false, std::string endpointName = "");
   ~WS_TCP_Provider() override;
   void run() override;
   void stop() override;

private:

   bool sendPacket(const void* conn, const unsigned char* payload, uint size) override;
   void closeConnection(const void* conn, const std::string& reason) override;

   Server             mServer;
   uint16_t           mListenPort;
   std::thread        mAsioThread;

   void onMonitorIteration();

   //websocket events
   void onWSMessage(ConnHandler chl, ServerMessagePtr msg);
   void onWSOpen(ConnHandler chl);
   void onWSClose(ConnHandler chl);
};
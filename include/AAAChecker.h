#pragma once
#include <map>
#include <mutex>
#include <memory>
#include <atomic>

class Router;
class PacketParser;

struct Profile
{
   explicit Profile(uint cid)
      : CID(cid), IsAuthenticated(false), IsConnected(true)
   { }

   uint CID;
   std::atomic<bool> IsConnected;
   std::atomic<bool> IsAuthenticated;
};

struct ProfileFactory
{
   virtual std::shared_ptr<Profile> createOne(uint cid)
   {
      return std::make_shared<Profile>(cid);
   }
};

class AAAChecker
{
public:
   explicit AAAChecker(Router* router) : mRouter(router)
   {
      mProfileFactory = std::make_unique<ProfileFactory>();
   }

   std::shared_ptr<const Profile> getProfile(uint CID);
   void setAuthFlag(std::shared_ptr<const Profile> client, bool flag);
   template <class T> void setProfileFactory() { mProfileFactory.reset(new T()); }
   void connected(uint CID);
   void disconnected(uint CID);

   virtual void onPacketFromUnidentified(std::shared_ptr<const Profile> client, PacketParser* reqPacket, PacketParser* repPacket);
   virtual ~AAAChecker() = default;
protected:
   Router* mRouter;
   std::unique_ptr<ProfileFactory> mProfileFactory;
   std::map<uint, std::shared_ptr<Profile>> mProfileMap;
   std::mutex mProfileLock;

   virtual void connectHandler(std::shared_ptr<const Profile> client);
   virtual void disconnectHandler(std::shared_ptr<const Profile> client) { }
};

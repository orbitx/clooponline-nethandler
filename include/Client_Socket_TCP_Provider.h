#pragma once

#include "NetProvider.h"
#include "clpbuffer.h"
#include <boost/asio.hpp>

class Client_Socket_TCP_Provider : public NetProvider
{
public:
   Client_Socket_TCP_Provider(uint8_t IDPostFix, Router* router, std::string hostip, uint16_t hostport);
   ~Client_Socket_TCP_Provider() override;

   void run() override;
   void stop() override;

private:
   bool sendPacket(const void* conn, const unsigned char* payload, uint size) override;
   void closeConnection(const void* conn, const std::string& reason) override;
   boost::system::error_code write(const unsigned char* payload, size_t len);
   void read();

   boost::asio::io_service mIOService;
   boost::asio::ip::tcp::socket mSocket;
   std::thread mIOServiceThread;
   std::string mHostIP;
   uint16_t mHostPort;
   clpbuffer mBuffer;
};